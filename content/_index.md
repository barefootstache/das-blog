---
title: "Welcome to Data Awareness & Sovereignity (DAS)! :tada:"
description: "This is the welcome page of Data Awareness & Sovereignity or short DAS."
---

{{< lead >}}
Own your own intrÄnet
{{< /lead >}}

Data Awareness and Sovereignty (DAS) is to give people the freedom to choose their own apps, so 
that they can become independent from big tech. Our solution is the unification of cloud and 
communication services which provide trust and sovereignty. You will have full ownership of your 
data by easily hosting your own free open source apps from the DAS Store. 

