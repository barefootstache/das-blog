[Demo](https://barefootstache.codeberg.page/das-blog/@pages/public/)

# DAS Blog

This static website generator uses [hugo](https://gohugo.io/) with the theme [congo](https://jpanther.github.io/congo/).

## Organising content

Make sure you are familiar with [organising the content](https://jpanther.github.io/congo/docs/getting-started/#organising-content)

## Remark

Due to current construction of GitLab pages, we have mirrored the pages to Codeberg and can be accessed 
[here](https://barefootstache.codeberg.page/das-blog/@pages/public/).

## Codeberg Pages Maintenance

The `pages` branch is used to reference the static site generation. This branch should only push the `public` directory.

Additionally each `index.html` file that references the home namely `DAS Blog` needs to mainly change its `href` to 

```
href="/das-blog/@pages/public/"
```

otherwise when the user clicks on the home link, it will break.
